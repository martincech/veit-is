(ns veit.utils.date-utils-test
  (:require [veit.utils.date-utils :as d]
            [java-time :as t]
            [expectations :refer :all]))

;; excel date to local date
(expect (t/local-date "1900-01-01") (d/excel-date->local-date 0))
(expect (t/local-date "1900-01-02") (d/excel-date->local-date 1))
(expect (t/local-date "2017-03-02") (d/excel-date->local-date 42794))

;; excel date to local date with default pattern
(expect "1900-01-01" (d/format-excel-date 0))
(expect "1900-01-02" (d/format-excel-date 1))
(expect "2017-03-02" (d/format-excel-date 42794))

;; excel date to local date with custom pattern
(expect "00 01 01" (d/format-excel-date "YY MM dd" 0))
(expect "00 01 02" (d/format-excel-date "YY MM dd" 1))
(expect "17 03 02" (d/format-excel-date "YY MM dd" 42794))
