(ns veit.modules.integration.abra.abra-client-test
  (:require
   [veit.modules.integration.abra.abra-client :as abra]
   [clj-http.fake :refer [with-fake-routes]]
   [expectations :refer :all]
   [clj-http.client :as http]))

(defn- generate-request
  ([action]
   (generate-request action {}))
  ([action params]
   (let [action-params (merge {:action action} params)]
     {:as :json
      :query-params action-params
      :content-type :json})))

(defn- fake-abra [action response-status]
  (with-fake-routes
    {{:address abra/abra-url
      :query-params {:action action}}
     (fn [request] {:status response-status :body "{}"})}
    (http/get abra/abra-url (generate-request action))))

(def tables [{:table_name "ORDERS1"
              :columns [{:column_name "ID"}
                        {:column_name "CODE"}
                        {:column_name "DESCRIPTION"}]}
             {:table_name "ORDERS"
              :columns [{:column_name "ID"}
                        {:column_name "CODE"}
                        {:column_name "ADDRESS"}]}
             {:table_name "ORDERING"
              :columns [{:column_name "ID"}
                        {:column_name "CUSTOMER"}]}])

(expect clojure.lang.ExceptionInfo
        (fake-abra "missingRequiredParamForAbraAction" 400))

(expect clojure.lang.ExceptionInfo
        (fake-abra "unknownAbraAction" 404))

(expect
 (second tables)
 (abra/get-table tables "ORDERS"))

(expect
 [(first tables) (second tables)]
 (abra/get-table-by-prefix (conj tables {:table_name "CARS"}) "ORDERS"))

;; you can get query plain-text result via
;; `(abra/abra-action! abra/get-query-result-action {:q "SELECT id, ordNumber, amount FROM  IssuedOrders ROWS 3"} :get :auto)`
(let [query "SELECT id, ordNumber, amount FROM IssuedOrders ROWS 3"
      raw-query-result
      "ID,ORDNUMBER,AMOUNT
1900000101,1,0
1B00000101,3,3048
2900000101,2,0"]

  (expect
    {:header ["ID", "ORDNUMBER" "AMOUNT"]
     :data [["1900000101" "1" "0"]
            ["1B00000101" "3" "3048"]
            ["2900000101" "2" "0"]]}

    (with-fake-routes
      {{:address abra/abra-url
        :query-params {:action "getQueryResult" :q query}}
       (fn [request] {:status 200 :body raw-query-result})}

      (abra/get-query-result query))))

(let [sql "UPDATE RandomOrders SET confirmed='A' WHERE ID='000999000999000'"]

  (expect
    {:ok "true"}
    (with-fake-routes
      {{:address abra/abra-url
        :query-params {:action "executeSql" :sql sql}}
       {:post (fn [request] {:status 200 :body "{\"ok\" : \"true\"}"})}}

      (abra/execute-sql sql))))
