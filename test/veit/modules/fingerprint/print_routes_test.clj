(ns veit.modules.fingerprint.print-routes-test
  (:require [cheshire.core :as json]
            [veit.handler :refer [app]]
            [peridot.core :as p]
            [expectations :refer :all]))

(defn- create-register-request
  [body]
  (-> (p/session app)
      (p/request "/api/fingerprints/register"
                 :request-method :post
                 :content-type "application/json"
                 :body body)
      :response))

(defn- get-parsed-body
  [response]
  (let [response-body (-> :body response slurp (json/parse-string true))]
    (println response-body)
    response))

(defn- request-missing-employee-id
  []
  (-> (json/generate-string {:print-marker "abcd"})
      create-register-request))

(defn- request-empty-finger-print-marker
  []
  (-> (json/generate-string {:print-marker "" :comment "aaa"})
      create-register-request))

(defn- request-register-new-fingerprint
  []
  (create-register-request
   (json/generate-string {:employee-id 1 :print-marker "aaa" :comment "blabla"})))

(defn- request-download-all
  []
  (-> (p/session app)
      (p/header "Accept" "application/json")
      (p/request "/api/fingerprints/prints"
                 :request-method :get
                 :content-type "application/json")
      :response))

;;; TODO: fix these tests
;;; Consider to move integrationt tests elsewhere
;;; We could add more lightweight tests here but not sure if it's worth the effort 

;; (expect (more-of response
;;                  400 (:status response)
;;                  "missing-required-key" (-> response
;;                                             get-parsed-body
;;                                             :errors
;;                                             :print-marker))
;;         (request-missing-employee-id))

;; (expect (more-of response
;;                  400 (:status response)
;;                  #"\(not \(blank" (-> response
;;                                       get-parsed-body
;;                                       :errors
;;                                       :print-marker))
;;         (request-empty-finger-print-marker))

;; (expect (more-of response
;;                  201 (:status response)
;;                  ;; TODO:
;;                  ;;   body should be nil ?
;;                  ;;   Location header should be returned pointing to the new entity 
;;                  nil (-> (request-register-new-fingerprint)
;;                          get-parsed-body
;;                          veit.modules.fingerprint.db.print-entity/register-print)))

;; (expect (more-of response
;;                  200 (:status response)
;;                  20 (-> response get-parsed-body count))
;;         (request-download-all))

