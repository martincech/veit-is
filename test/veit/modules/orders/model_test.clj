(ns veit.modules.orders.model-test
  (:require [clojure.java.io :as io]
            [clojure.string :as s]
            [expectations :refer [expect]]
            [veit.modules.orders.model :as m]))

(def order-id "1U31000101")

(defn- mock-order-data [select]
  (condp = select

    (m/read-sql "order.sql" order-id)
    {:header ["ORDER_ID" "DOC_QUEUE_CODE" "ORDER_NUMBER" "CONFIRMED" "PERIOD_CODE" "CURRENCY_CODE" "PRICE_WITH_VAT" "PRICE_WITHOUT_VAT"
              "PRICE_IN_LOCAL_CURRENCY" "CREATED_DATE" "SUPPLIER_CODE" "SUPPLIER_NAME" "SUPPLIER_IDENTIFICATION_NUMBER" "SUPPLIER_VAT_ID"
              "SUPPLIER_CITY" "SUPPLIER_STREET" "SUPPLIER_POSTCODE" "SUPPLIER_COUNTRY" "SUPPLIER_COUNTRY_CODE" "SUPPLIER_PHONE"
              "SUPPLIER_EMAIL" "CLIENT_CITY" "CLIENT_STREET" "CLIENT_POSTCODE" "CLIENT_COUNTRY" "CLIENT_COUNTRY_CODE"
              "CLIENT_PHONE" "CLIENT_EMAIL"],
     :data [["1U31000101" "OV1" "1114" "A" "2016" "CZK" "4296" "3550,39" "4296" "42675" "00466" "LASMETAL s.r.o."
             "26957833" "CZ26957833" "Vyškov" "Tržiště 234/73" "682 01" "Česká republika" "CZ" "+420 517 330 892"
             "heger@lasmetal.cz" "Moravany" "Modřická 700/52" "664 48" "Czech Republic" "CZ" "+420 545 235 252" ""]]}

    (m/read-sql "order_items.sql" order-id)
    {:header ["POSITION_NUM" "CODE" "NAME" "FOREIGNNAME" "DESCRIPTION" "QUANTITY" "UNIT_PRICE" "PRICE_FOR_ALL_ITEMS"
              "TOTAL_PRICE_WITH_VAT" "TOTAL_PRICE_WITHOUT_VAT" "DISCOUNT_PERCENT" "VAT_RATE_PERCENT" "DELIVERY_DATE"],
     :data [["1" "UAUN0015" "Nosné prvky auto l-7340mm, 300-80mm, zadní L-110mm" "English name" "(WideMemo)" "1" "2962" "2962"
             "3584,02" "2962" "0" "21" "42691"]
            ["2" "UAUN0011" "Zadní úhelník 110mm pro závěs" "" "(WideMemo)" "1" "588,5" "588,5" "712,09" "588,5" "0" "21" "42691"]]}))

(def expected-order
  {:order {:priceWithoutVat "3550,39",
           :orderNumber "OV1-1114/2016",
           :confirmed "A",
           :priceWithVat "4296",
           :priceWithVatInLocalCurrency "4296",
           :id "1U31000101",
           :createdDate "2016-11-03",
           :currencyCode "CZK",
           :items [{:description "(WideMemo)",
                    :vatRatePercent "21",
                    :name "Nosné prvky auto l-7340mm, 300-80mm, zadní L-110mm",
                     ;; :name "English name",
                    :discountPercent "0",
                    :priceForAllItems "2962",
                    :totalPriceWithVat "3584,02",
                    :code "UAUN0015",
                    :position "1",
                    :totalPriceWithoutVat "2962",
                    :quantity "1",
                    :unitPrice "2962",
                    :deliveryDate "2016-11-19"}
                   {:description "(WideMemo)",
                    :vatRatePercent "21",
                    :name "Zadní úhelník 110mm pro závěs",
                    :discountPercent "0",
                    :priceForAllItems "588,5",
                    :totalPriceWithVat "712,09",
                    :code "UAUN0011",
                    :position "2",
                    :totalPriceWithoutVat "588,5",
                    :quantity "1",
                    :unitPrice "588,5",
                    :deliveryDate "2016-11-19"}],
           :deliveryDate "2016-11-19"},
   :supplier {:code "00466",
              :name "LASMETAL s.r.o.",
              :identificationNumber "26957833",
              :vatId "CZ26957833",
              :address {:city "Vyškov",
                        :street "Tržiště 234/73",
                        :postalCode "682 01",
                        :country "Česká republika",
                        :countryCode "CZ",
                        :phone "+420 517 330 892",
                        :email "heger@lasmetal.cz"}},
   :client {:address {:city "Moravany",
                      :street "Modřická 700/52",
                      :postalCode "664 48",
                      :country "Czech Republic",
                      :countryCode "CZ",
                      :phone "+420 545 235 252",
                      :email ""}}})

(expect 
  expected-order
  (m/get-order mock-order-data order-id "cs"))

(expect
  (assoc-in expected-order [:order :items 0 :name] "English name")
  (m/get-order mock-order-data order-id "en"))

(expect
 (assoc-in expected-order [:order :items 0 :name] "English name")
 (m/get-order mock-order-data order-id "de"))


(expect
 (s/starts-with? (m/read-sql "order_items.sql" order-id) "SELECT"))
(expect
 (s/starts-with? (m/read-sql "order.sql" order-id) "SELECT"))
