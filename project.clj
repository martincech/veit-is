(defproject
  veit-is
  "0.1.0-SNAPSHOT"
  :description
  "Information system for Veit company"
  :url
  "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies
  [[org.clojure/clojure "1.9.0-alpha14"]
   [prone "1.1.4"]
   [pandect "0.6.0"]
   [noir-exception "0.2.5"]
   [com.taoensso/timbre "4.7.4"]
   [org.postgresql/postgresql "9.3-1102-jdbc41"]
   [lib-noir "0.9.9"]
   [environ "1.1.0"]
   [ring "1.5.0"]
   [im.chit/cronj "1.4.4"]
   [compojure "1.5.1"]
   [metosin/compojure-api "1.2.0-alpha1"]
   [metosin/ring-http-response "0.8.0"]
   [clj-http "3.4.1"]
   [korma "0.4.3"]
   [migratus "0.8.32"]
   [clj-time "0.12.2"] ; needed by compojure-api
   [com.novemberain/validateur "2.5.0"]
   [slingshot "0.12.2"]
   [cheshire "5.6.3"]
   [ring-cors "0.1.9"]
   [org.clojure/data.csv "0.1.3"]
   [clojure.java-time "0.2.2"]
   [org.tobereplaced/http-accept-headers "0.1.0"]]
  :jvm-opts
  ["-server"]
  :main veit.core
  :resource-paths
  ["resources" "src/migrations"]
  :plugins
  [[lein-ring "0.10.0"]
   [lein-elastic-beanstalk "0.2.8-SNAPSHOT"]
   [lein-environ "1.0.0"]]
  :ring
  {:handler veit.handler/app,
   :init veit.handler/init,
   :destroy veit.handler/destroy
   :uberwar-name "veit-is.war"}
  :profiles
  {:uberjar {:omit-source true
             :aot :all
             :uberjar-name "veit-is.jar"
             :env {
               :abraurl "http://intranet.veit.cz/ARESTD/API/RUN"}}
   :uberwar {:omit-source true
             :aot :all
             :uberwar-name "veit-is.war"
             :env {
               :abraurl "http://intranet.veit.cz/ARESTD/API/RUN"}}
   :dev {:dependencies [[expectations "2.2.0-beta1"]
                        [peridot "0.4.4"]
                        [clj-http-fake "1.0.3"]]
         :ring {:open-browser? false}
         :plugins [[lein-expectations "0.0.8"]
                   [lein-autoexpect "1.9.0"]]
         :env {:db-user "veit_is"
               :db-password "veit_is"
               :db-host "localhost"
               :db-name "veit_is"
               :abraurl "http://intranet.veit.cz/ARESTD/API/RUN"}}      
   })
