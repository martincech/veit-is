(ns veit.modules.integration.abra.abra-client
  (:require [clj-http.client :as http]
            [clojure.data.csv :refer [read-csv]]
            [clojure.string :refer [split join]]
            [environ.core :refer [env]]
            [slingshot.slingshot :refer [throw+ try+]]
            [taoensso.timbre :as timbre]))

(def abra-url (env :abraurl))
(def get-db-schema-action "getDbSchema")
(def get-query-result-action "getQueryResult")
(def execute-sql-action "executeSql")
(def confirm-order-action "orderMailConfirmation")

(def abra-action-types
  {:get http/get
   :post http/post})

(defn abra-action!
  "Calls ABRA API action by its name.
   Optional request params can be specified as a second argument.
   Third argument give an option to customize output format - default is :json"
  ([action]
   (abra-action! action {}))
  ([action params]
   (abra-action! action params :get))
  ([action params action-type]
   (abra-action! action params action-type :json))
  ([action params action-type output-format]
   (let [action-params (merge {:action action} params)]
     (timbre/info "Calling ABRA action on URL" abra-url)
     (try+
      (:body ((abra-action-types action-type) abra-url
                       {:query-params action-params
                        :as output-format
                        :content-type :json}))
      (catch [:status 400] {:keys [body]}
        (timbre/error "Bad request from application" body)
        (throw+ {:type :user-error :message body}))
      (catch [:status 401] {:keys [body]}
        (timbre/error "User has not been authorized" body)
        (throw+ {:type :user-error :message body}))
      (catch [:status 403] {:keys [body]}
        (timbre/error "User doesn't have permission" body)
        (throw+ {:type :user-error :message body}))
      (catch [:status 404] {:keys [body]}
        (timbre/error "Action doesn't exists" body)
        (throw+ {:type :user-error :message body}))
      (catch Object _
        (timbre/error (:throwable &throw-context) "unknown error"))))))

(defn get-abra-tables
  "Lists abra tables"
  []
  (abra-action! get-db-schema-action))

(defn get-table
  "Find table by exact name.
   You can use 2-arg version if you already have tables loaded in local var."
  ([table-name]
   (get-table (get-abra-tables) table-name))
  ([tables table-name]
   (first (filter #(= (:table_name %) (clojure.string/upper-case table-name)) tables))))

(defn get-table-by-prefix
  "Similar to `get-table` but find all possible matches by checking if table name starts with given prefix.
   Unlike `get-table` this function returns collection even if it returns only single table!"
  ([table-name]
   (get-table-by-prefix (get-abra-tables) table-name))
  ([tables table-name]
   (filter #(.startsWith (:table_name %) table-name) tables)))

(defn print-table-columns
  "Print each table column on a new line.
   You can pass existing tables structure - in that case no api is called.
   Otherwise, the `get-abra-tables` is called internally."
  ([table-name]
   (print-table-columns (get-abra-tables) table-name))
  ([tables table-name]
   (let [table (get-table tables table-name)]
     (doseq [col (:columns table)] (println (:column_name col))))))

(defn- parse-query-result
  "Parses result of SQL query execution which is just a multiline string
  with first line representing header and next lines representing data (each line one row.)"
  [result-string]
  (let [[header & data] (read-csv result-string)]
    {:header header
     :data data}))

(defn get-query-result
  "Executes given SQL query via internal ABRA API action GETSQLQUERY
  and returns result {:header column-names :data rows}."
  [query]
  ;; we use :auto output coercion because the response from getQueryResult function is text/plain
  ;; (list of rows with columns separated by semicolons)
  (parse-query-result (abra-action! get-query-result-action {:q query} :get :auto)))

(defn print-query-result
  "Helper function for pretty printing the result of `get-query-result` function."
  [query]
  (let [{:keys [header data]} (get-query-result query)]
    (println header)
    (doseq [row data]
      (println row))))

(defn execute-sql
  "Executes given SQL statement (update, insert, or delete) via internal ABRA API action EXECUTESQL.
  The result of executed statement is typically just `{\"ok\" : \"true\"}`, nothing more.
  If you need more data, you'll have to issue another SELECT via `get-query-result`.

  Beware that this is an unsafe operation which may result in data loss.
  Use with care!"
  [sql]
  (abra-action! execute-sql-action {:sql sql} :post))


;;; Useful shortcuts
(def pq print-query-result)
(defn t [tables table-name] (get-table tables table-name))
(defn ptc [tables table-name] (print-table-columns tables table-name))

