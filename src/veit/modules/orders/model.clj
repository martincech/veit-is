(ns veit.modules.orders.model
  "Data fetching layer for orders."
  (:require [clojure.java.io :as io]
            [clojure.string :as s]
            [java-time :as t]
            [veit.utils.date-utils :as d]
            [veit.modules.integration.abra.abra-client
            :refer
            [abra-action! confirm-order-action]]))

(defn read-sql [filename order-id]
  (-> (str  "sql/orders/" filename)
      io/resource
      slurp
      ;; remove comments
      (s/replace #"--.*\n" "")
      (s/replace ":order-id" order-id)
      )
  )

(defn- order-details-select [order-id] (read-sql "order.sql" order-id))

(defn- order-items-select [order-id] (read-sql "order_items.sql" order-id))

(defn- format-date [date]
  (when date
    (d/format-excel-date (Double/valueOf (clojure.string/replace date #"[,]" ".")))))

(defn- get-order-delivery-date
  "Finds the single delivery date for whole order based on max delivery date among all order items."
  [order-items]
  (->>
   (map (fn [item] (t/local-date (:deliveryDate item)))
        order-items)
   sort
   last
   t/format))

(defn- convert-order-data
  [named-data order-items]
  {:id (named-data "ORDER_ID")
   :orderNumber (str (named-data "DOC_QUEUE_CODE")
                     "-"
                     (named-data "ORDER_NUMBER")
                     "/"
                     (named-data "PERIOD_CODE"))
   :confirmed (named-data "CONFIRMED")
   :currencyCode (named-data "CURRENCY_CODE")
   :priceWithVat (named-data "PRICE_WITH_VAT")
   :priceWithoutVat (named-data "PRICE_WITHOUT_VAT")
   :priceWithVatInLocalCurrency (named-data "PRICE_IN_LOCAL_CURRENCY")
   :createdDate (format-date (named-data "CREATED_DATE"))
   :deliveryDate (get-order-delivery-date order-items)
   :items order-items})

(defn- convert-supplier-data
  [named-data]
  {:code (named-data "SUPPLIER_CODE")
   :name (named-data "SUPPLIER_NAME")
   :identificationNumber (named-data "SUPPLIER_IDENTIFICATION_NUMBER")
   :vatId (named-data "SUPPLIER_VAT_ID")
   :address {:city (named-data "SUPPLIER_CITY")
             :street (named-data "SUPPLIER_STREET")
             :postalCode (named-data "SUPPLIER_POSTCODE")
             :country (named-data "SUPPLIER_COUNTRY")
             :countryCode (named-data "SUPPLIER_COUNTRY_CODE")
             :phone (named-data "SUPPLIER_PHONE")
             :email (named-data "SUPPLIER_EMAIL")}})

(defn- convert-client-data
  [named-data]
  {:address {:city (named-data "CLIENT_CITY")
             :street (named-data "CLIENT_STREET")
             :postalCode (named-data "CLIENT_POSTCODE")
             :country (named-data "CLIENT_COUNTRY")
             :countryCode (named-data "CLIENT_COUNTRY_CODE")
             :phone (named-data "CLIENT_PHONE")
             :email (named-data "CLIENT_EMAIL")}})

(defn- convert-order
  "Converts data received as a result of SQL query to the appropriate JSON structure converting necessary fields.
  Conversion rules:
  - order number: is a composition of DOC_QUEUE_CODE, ORDER_NUMBER, and PERIOD_CODE; e.g. 'OV1-1114/2016'
  - dates: received from Abra in 'Excel date' format (number of days from 1.1.1900) -> they are converted to the form 'YYYY-MM-dd'
  - order delivery date: computed field; it's the largest delivery in any of order items"
  [header order-data order-items]
  (let [named-data (zipmap header order-data)]
    {:order (convert-order-data named-data order-items)
     :supplier (convert-supplier-data named-data)
     :client (convert-client-data named-data)}))

(defn- convert-order-item
  [header order-item-data language]
  (let [named-data (zipmap header order-item-data)]
    {:position (named-data "POSITION_NUM")
     :code (named-data "CODE")
     ;; use foreign name for languages other than "cs" but only if foreign name is not empty
     :name (if (and (not= "cs" language)
                    (not (s/blank? (named-data "FOREIGNNAME"))))
             (named-data "FOREIGNNAME")
             (named-data "NAME"))
     :description (named-data "DESCRIPTION")
     :quantity (named-data "QUANTITY")
     :unitPrice (named-data "UNIT_PRICE")
     :priceForAllItems (named-data "PRICE_FOR_ALL_ITEMS")
     :totalPriceWithVat (named-data "TOTAL_PRICE_WITH_VAT")
     :totalPriceWithoutVat (named-data "TOTAL_PRICE_WITHOUT_VAT")
     :discountPercent (named-data "DISCOUNT_PERCENT")
     :vatRatePercent (named-data "VAT_RATE_PERCENT")
     :deliveryDate (format-date (named-data "DELIVERY_DATE"))}))

(defn- get-order-items
  "Return all order items.
  If order doesn't exist then nil is returned."
  [get-query-result-fn order-id language]
  (let [{:keys [header data]} (get-query-result-fn (order-items-select order-id))]
    (map #(convert-order-item header % language) data)))

(defn get-order
  "Return all details about order.
  If order doesn't exist then nil is returned.
  The language parameter is important for returning names of order items in proper language.
  Currently only English and Czech languages are supported:
  - If Czech (\"cs\") language is requested, then czech name is returned
  - If English (\"en\") or any other language is  requested, then English name is returned if it's not empty,
    otherwise the Czech name is returned."
  [get-query-result-fn order-id language]
  (let [{:keys [header data]} (get-query-result-fn (order-details-select order-id))
        order-data (first data)
        order-items (get-order-items get-query-result-fn order-id language)]
    (when order-data
      (do
        order-data
        (convert-order header order-data order-items)))))

(defn confirm-order!
  [order-id]
  (abra-action! confirm-order-action {:orderid order-id} :post))

