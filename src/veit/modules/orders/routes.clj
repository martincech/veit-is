(ns veit.modules.orders.routes
  (:require [compojure.api.sweet :refer [context GET POST]]
            [org.tobereplaced.http-accept-headers :as accept-headers]
            [ring.util.http-response :as response]
            [schema.core :as s]
            [veit.modules.integration.abra.abra-client
             :refer
             [execute-sql get-query-result]]
            [veit.modules.orders.model :as m]))

(defn- get-preferred-language [accept-language-header]
  (let [accepted-locales (accept-headers/parse-accept-language [] accept-language-header)
        preferred-locale (first accepted-locales)
        ;; the language itself is just the first part of locale
        ;; locale can be simple like "en" in which case the language is the same as locale
        ;; or it can be more complex like "en-US" (language-country) in which case the language is only the "en" part
        preferred-language (when preferred-locale (re-find #"[a-z]+" preferred-locale))]
    preferred-language))

(defn- get-order-detail
  [order-id accept-language-header]
  (response/ok (m/get-order get-query-result
                            order-id
                            (get-preferred-language accept-language-header))))

(defn- confirm-order
  [order-id]
  ;; confirm-order doesn't have any meaningful result
  (m/confirm-order! order-id)
  (response/ok (str "/api/orders/" order-id)))

(def orders-routes
  (context "/orders" []
    :tags ["Orders"]

    (GET "/:order-id" [order-id :as request]
      :path-params [order-id :- s/Str]
      :responses {200 {:description "Request was processed without errors."}}
      :summary    "Returns detail info about given order."
      (get-order-detail order-id (get-in request [:headers "accept-language"])))
    (POST "/:order-id/confirm" order-id
      :path-params [order-id :- s/Str]
      :responses {200 {:description "Request was processed without errors."}}
      :summary    "Final order confirmation."
      (confirm-order order-id))))
