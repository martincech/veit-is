(ns veit.modules.fingerprint.routes.print-routes
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :as response]
            [schema.core :as s]
            [taoensso.timbre :as timbre]
            [veit.modules.fingerprint.db.print-entity :as db]
            [veit.modules.fingerprint.schema.fingerprint :refer [Fingerprint]]
            [veit.schema.common :refer [Message]]))

(defn- get-location-url
  [request]
  (str (:server-name request) ":" (:server-port request) (:uri request)))

(defn register-print
  [request print-record]
  (let [id (-> (db/register-print print-record) :id)]
    (timbre/info "Saved new fingerprint record to the DB")
    (response/created (str (get-location-url request) "/" id)
                      {:message "New fingerprint has been saved"})))

(defn- download-all-prints
  [assigned revoked]
  (timbre/info "Getting all available fingerprint records")
  (let [result (db/get-all-prints assigned revoked)]
    (if-not (empty? result)
      (response/ok (into [] result))
      (response/no-content))))

(defn- assign-print
  [print-id user-id]
  (timbre/info "Assigning  print with id" print-id "to user" user-id)
  (let [num-updated (db/assign-print print-id user-id)]
    (if (= num-updated 0)
      (response/not-found {:message "Fingerprint doesn't exist."})
      (response/ok {:message "Print has been assigned"}))))

(defn- revoke-print
  [print-id]
  (timbre/info "Revoking print with id" print-id "")
  (let [num-updated (db/revoke-print print-id)]
    (if (= num-updated 0)
      (response/bad-request {:message "Fingerprint doesn't exist or haven't been assigned yet."})
      (response/ok {:message "Print has been succesfully revoked"}))))

(def print-routes
  (context "/fingerprints" []
    :tags ["Fingerprint"]

    (POST "/register" {:as request}
      :body       [print-record Fingerprint]
      :responses {201 {:schema Message}}
      :summary "Registers new fingerprint with a comment."
      (register-print request print-record))

    (context "/prints" []

      (GET "/" {:as request}
        :query-params [{assigned :- (describe s/Bool "Default value is true." :default true) true}
                       {revoked :- (describe s/Bool "Default values is false" :default false) false}]
        :responses {200 {:description "Returns all saved fingerprints matching the parameters."}
                    204 {:description "No matching fingerprints."}}
        :summary    "Returns all available prints with their corresponding employee data."
        (download-all-prints assigned revoked))

      (POST "/:id/assign" {:as request}
        :path-params [id :- s/Int]
        :body [user-id s/Int]
        :responses {200 {:schema Message}
                    404 {:schema Message}}
            ;TODO swagger not generating all responses, just last one
        :summary "Assigns user to a newly registered fingerprint."
        (assign-print id user-id))

      (POST "/:id/revoke" {:as request}
        :path-params [id :- s/Int]
        :responses {200 {:schema Message}
                    400 {:schema Message}}
        :summary "Revokes access for assigned fingerprint."
        (revoke-print id)))))
