(ns veit.modules.fingerprint.db.print-entity
  (:require [korma.core :refer :all]
            [veit.config.common-config :refer [db-config]]))

(defentity print-entity
  (table :fingerprint)
  (database db-config))

(defn register-print
  "Saves fingerprint with comment to the DB"
  [print-record]
  (insert print-entity
          (values {:comment (:comment print-record)
                   :print_marker (:print-marker print-record)})))

(defn get-all-prints
  "Gets all recorded prints"
  [assigned revoked]
  (select print-entity
          (where (and
                  (= :revoked revoked)
                  (and (not= :user_id nil)
                       assigned)))))

(defn assign-print
  "Assigns print to the user"
  [print-id user-id]
  (update print-entity
          (set-fields {:user_id user-id})
          (where (= :id print-id))))

(defn revoke-print
  "Revokes already registered print"
  [print-id]
  (update print-entity
          (set-fields {:revoked true})
          (where (and (= :id print-id)
                      (not= :user_id nil)))))
