(ns veit.modules.fingerprint.schema.fingerprint
  (:require [clojure.string :refer [blank?]]
            [schema.core :as s]))

(def error-message 'blank)

(s/defschema Fingerprint
  {:comment s/Str
   :print-marker (s/conditional (comp not blank?) s/Str error-message)})
; TODO better conditional and exception message
