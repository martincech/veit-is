(ns veit.modules.user.user-model)

(def mocked-users
  [{:id 0 :first-name "Robt" :surname "Caprio"}
   {:id 1 :first-name "Romaine" :surname "Sprague"}
   {:id 2 :first-name "Ivette" :surname "Grunewald"}
   {:id 3 :first-name "Terrell" :surname "Rhymer"}
   {:id 4 :first-name "Lennie" :surname "Paquin"}])

(defn get-all-users
  "Gets all users from the DB"
  []
  mocked-users)
