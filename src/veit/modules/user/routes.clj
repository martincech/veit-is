(ns veit.modules.user.routes
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :as response]
            [taoensso.timbre :as timbre]
            [veit.modules.user.user-model :as users]))

(defn- get-all-users
  []
  (timbre/info "Returning list of all users")
  (response/ok (users/get-all-users)))

(def user-routes
  (context "/users" []
    :tags ["Users"]

    (GET "/" {:as request}
      :responses {200 {:description "Request was processed without errors."}}
      :summary    "Returns all users in case of success."
      (get-all-users))))
