(ns veit.modules.completion-norms.routes
  (:require [compojure.api.sweet :refer [GET context]]
            [ring.util.http-response :as response]
            [veit.modules.completion-norms.model :as model]
            [veit.modules.integration.abra.abra-client :refer [abra-action!]]))

(defn- get-completion-norm
  [code]
  (response/ok (model/get-completion-norm abra-action! code)))

(defn- get-completion-norm-items
  [code]
  (response/ok (model/get-completion-norm-items abra-action! code)))

(def completion-norm-routes
  (context "/completionNorms" []
    :tags ["Completion Norms"]

    (GET "/:norm-code" [norm-code]
      :responses {200 {:description "Request was processed without errors."}}
      :summary    "Returns a completion norm identified by its unique code."
      (get-completion-norm norm-code))
    (GET "/:norm-code/items" [norm-code]
      :responses {200 {:description "Request was processed without errors."}}
      :summary    "Returns a completion norm's items using completion norm's."
      (get-completion-norm-items norm-code))))
