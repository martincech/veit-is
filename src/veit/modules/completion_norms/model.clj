(ns veit.modules.completion-norms.model)

(defn get-completion-norm
  "Finds a completion norm aka 'assembling standard' by its code.
  Accepts function to invoke abra-action as a first parameter and completion norm's code as a second parameter."
  [abra-action norm-code]
  (abra-action "getCompletionNorm" {:normCode norm-code}))


(defn get-completion-norm-items
  "Get all items (subparts) of completion norm with given code.
  Accepts function to invoke abra-action as a first parameter and completion norm's code as a second parameter."
  [abra-action norm-code]
  (abra-action "getCompletionNormItems" {:normCode norm-code}))
