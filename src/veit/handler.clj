(ns veit.handler
  (:require [compojure.api.sweet :refer [context defapi]]
            [cronj.core :as cronj]
            [environ.core :refer [env]]
            [migratus.core :as migratus]
            [ring.middleware.cors :refer [wrap-cors]]
            [taoensso.timbre :as timbre]
            [veit.config
             [common-config :refer [db-config]]
             [exceptions :as exceptions]
             [swagger-config :refer [swagger-config]]]
            [veit.modules.completion-norms.routes :refer [completion-norm-routes]]
            [veit.modules.fingerprint.routes.print-routes :refer [print-routes]]
            [veit.modules.completion-norms.routes :refer [completion-norm-routes]]
            [veit.modules.orders.routes :refer [orders-routes]]
            [veit.modules.user.routes :refer [user-routes]]
            [veit.session-manager :as session-manager]
            [veit.smoketest.routes :refer [smoke-tests-routes]]))

(def migratus-config
  {:store :database
   :migration-dir "migrations"
   :init-script "init.sql"
   :db db-config})

(defn- migrate-db
  []
  (timbre/info "Checking migrations")
  (try
    (migratus/init migratus-config)
    (migratus/migrate migratus-config)
    (catch Exception e (timbre/error "Failed to migrate DB" e)))
  (timbre/info "Finished migration"))

(defn init
  "init will be called once when
   app is deployed as a servlet on
   an app server such as Tomcat
   put any initialization code here"
  []
  ;; add logging configuration

  (migrate-db)
  ;;start the expired session cleanup job
  (cronj/start! session-manager/cleanup-job)
  (timbre/info "\n-=[ Veit IS started successfully"
              (when (env :dev) "using the development profile") "]=-"))

(defn destroy
  "destroy will be called when your application
   shuts down, put any clean up code here"
  []
  (timbre/info "Veit IS is shutting down...")
  (cronj/shutdown! session-manager/cleanup-job)
  (timbre/info "shutdown complete!"))

(defn cors [handler]
  (fn [request]
    (-> request
        handler
        (update :headers into {"Access-Control-Allow-Origin" "*"
                               "Access-Control-Allow-Mehotds" "GET, POST, PUT, DELETE, OPTIONS, HEAD"}))))
(defn allow-cross-origin  
  "middleware function to allow crosss origin"  
  [handler]    
  (fn [request]  
   (let [response {}]
    (-> response
        (assoc-in [:headers "Access-Control-Allow-Origin"]  "*")
        (assoc-in [:headers "Access-Control-Allow-Methods"] "GET,PUT,POST,DELETE,OPTIONS")
        (assoc-in [:headers "Access-Control-Allow-Headers"] "X-Requested-With,Content-Type,Cache-Control")))))

(defapi app {:format [:json-kw :transit-json]
             :swagger swagger-config
             :exceptions {:handlers {:compojure.api.exception/default
                                     exceptions/default-server-error-handler}}}
  (context
    "/api"
    []
    ;; TODO: wrap-cors middleware doesn't work for some reason
    ;; required headers are not added and browser fails with error:
    ;; "No 'Access-Control-Allow-Oriring' header is present on the requested resource."
    ;;:middleware [(wrap-cors 
;;      :access-control-allow-origin #".+" 
  ;;    :access-control-allow-methods [:get :put :post :delete])]
    #_(:middleware [[wrap-cors
                     :access-control-allow-origin ["*"]
                     :access-control-allow-methods [:get :put :post :delete]]])
    :middleware [cors]

    print-routes
    user-routes
    completion-norm-routes
    orders-routes
    smoke-tests-routes))
