(ns veit.config.exceptions
  (:require [ring.util.http-response :refer [internal-server-error]]
            [taoensso.timbre :as timbre]))

(defn default-server-error-handler [^Exception e data request]
  (timbre/error e data)
  (internal-server-error "Something bad happened."))
