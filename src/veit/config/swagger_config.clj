(ns veit.config.swagger-config)

(def swagger-config
  {:ui "/doc"
   :spec "/swagger.json"
   :data {:info {:title "Veit IS"
                 :version "0.1.0"
                 :description "Veit IS"
                 :tags [{"Fingerprint" "API for registering and getting employees fingerprints"}]}}})
