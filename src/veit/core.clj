(ns veit.core
  (:require [ring.adapter.jetty :as jetty]
            [veit.handler :refer [app]])
  (:gen-class))

(defn -main []
  (jetty/run-jetty
   app
   {:port 3000
    :join? false}))
