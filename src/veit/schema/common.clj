(ns veit.schema.common
  (:require [schema.core :as s]))

(s/defschema Message
  {:message s/Str})
