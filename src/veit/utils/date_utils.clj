(ns veit.utils.date-utils
  "Date handling utility functions."
  (:require [java-time :as t]))

(defn excel-date->local-date
  "Converts 'Excel date' (i.e. number of days from 01-01-1900) to java LocalDate."
  [excel-days]
    (t/minus (t/plus (t/local-date "1900-01-01") (t/days excel-days)) (t/days 2)) 
)

(defn format-excel-date
  "Formats date in excel format (i.e. number of days from 01-01-1900) to specified format.
   Most typical pattern 'yyyy-MM-dd' is covered by single argument version of this function."
  ([excel-date]
   (format-excel-date "yyyy-MM-dd" excel-date))
  ([date-pattern excel-date]
   (t/format date-pattern (excel-date->local-date excel-date))))

