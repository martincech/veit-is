(ns veit.smoketest.routes
  (:require [compojure.api.sweet :refer [GET context]]
            [ring.util.http-response :as response]
            [taoensso.timbre :as timbre]))

(def smoke-tests-routes
  (context "/smoketests" []
    :tags ["Smoke tests"]

    (GET "/ping" {:as request}
      :responses {200 {:description "Request was processed without errors."}}
      :summary    "Simple ping resource for checking if API is alive."
      {:status 200
       :body "OK"
       :headers {"Content-Type" "text/plain"}})))

