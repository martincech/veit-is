-- Creates emails table
CREATE TABLE IF NOT EXISTS fingerprint
(id SERIAL PRIMARY KEY,
 user_id INTEGER,
 comment VARCHAR(255) NOT NULL,
 print_marker VARCHAR(1000) NOT NULL,
 revoked BOOLEAN DEFAULT FALSE);
