# Veit IS

## Prerequisites

Created database with a user on the target host. Application is currently running on Postgres so it can be created with
following commands.

```
CREATE DATABASE veit_is;
CREATE USER veit_is WITH PASSWORD 'veit_is';
GRANT ALL PRIVILEGES ON DATABASE veit_is TO veit_is;
```

Application can obtain DB credentials and host from environment or Java variables or it will fallback to defaults
(everything is `veit_is`) then.

## Documentation

Documentation is automatically generated via Swagger so all you need is to run `lein ring server` and enjoy docs  on
`<your-host>:3000/doc/index.html`

## Running

To start a web server for the application, run:

    lein ring server

or use explicitly development environment via:

    lein with-profile dev ring server
    
For production environment use 
    
    lein uberjar 

to create a jar file and then

    java -jar .\target\veit-is.jar
    
## Deployment

There's [deploy.sh](scripts/deploy.sh) script for semi-automatic deployment.



## Code style
To ensure uniform code style in a codebase, we use following leiningen plugins:

* [lein-bikeshed](https://github.com/dakrone/lein-bikeshed)
    * `lein bikeshed -m 120` (line width: 120 chars)
    * basic style check
* [cljfmt](https://github.com/weavejester/cljfmt)
    * `lein cljfmt check`
    * Clojure file formatter
    * also as a plugin for VIM
        * integration with Emacs CIDER is unclear
        * integration with Cursive is probably non-existent
        * TODO: we should if this plugin is compatible with all IDEs and make the corrections if needed
* [kibit](https://github.com/jonase/kibit)
    * `lein kibit`
    * static code analyzer for Clojure and ClojureScript
* [eastwood](https://github.com/jonase/eastwood)
    * `lein eastwood`
    * Clojure lint tool
* [slamhound](https://github.com/technomancy/slamhound)
    * lein slamhound src/veit/<my_file>.clj
    * there are also plugins for Emacs and VIM
    * there's probably no plugin for Cursive

You should add them to your ~/.lein/profiles.clj
```
{:user {:plugins [
                  [jonase/eastwood "0.2.3"]
                  [lein-kibit "0.1.2"]
                  [lein-cljfmt "0.5.6"]
                  [lein-bikeshed "0.4.1"]
        :dependencies [[slamhound "1.5.5"]]
        :aliases {"slamhound" ["run" "-m" "slam.hound"]}}}

```

And then make sure that your code conforms to code style defined by these plugins.

You can use [check_code_style.sh](scripts/check_code_style.sh) script for running all these plugins at once.
