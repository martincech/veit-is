package cz.veit.is.confluence.completionnorm;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;

import java.util.Map;

/**
 * Macro showing the completion norm items tree via iframe.
 * It just calls the remote web application which returns the complete html and displays that html in iframe.
 *
 * In order to work, system environment variable "CONFLUENCE_COMPLETION_NORM_BASE_URL" must be defined.
 * It can be sth like this: "http://testis.veit.cz/completionnorms"
 */
public class CompletionNormMacro implements Macro {

    private static final String DEFAULT_COMPLETION_NORM_BASE_URL = "http://localhost:3001/completionNorms";
    private static final String COMPLETION_NORM_BASE_URL = getCompletionNormsBaseUrl();

    public String execute(Map<String, String> parameters, String s, ConversionContext conversionContext) throws MacroExecutionException {
        return String.format(
                "<iframe src=\"%s/%s\" noborder=\"0\" width=\"830\" height=\"800\" scrolling=\"yes\" seamless></iframe>",
                COMPLETION_NORM_BASE_URL,
                parameters.get("completion-norm-code"));
    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }


    private static String getCompletionNormsBaseUrl() {
        final String urlFromEnv = System.getenv("CONFLUENCE_COMPLETION_NORM_BASE_URL");
        if (urlFromEnv == null || urlFromEnv.length() == 0) {
            return DEFAULT_COMPLETION_NORM_BASE_URL;
        }
        return urlFromEnv;
    }
}
