#!/usr/bin/env bash
set -e
# uncomment for debugging
#set -x

## Script for deploying veit-is to the server.
##
## This file also contains necessary one-time setup for creating a veit-is system user and appropriate directory structure
## The commands are valid for Linux - CentOS 7. On other systems you might need to use different commands.
## 
## Run this script from project root foolder: `./scripts/deploy.sh`
## Make sure you're connected to VPN!

HOSTNAME=${1:-testis.veit.cz}
# TODO: using port different from 3000 requires changes in application configuration
ABRAURL=${2:-http://intranet.veit.cz/ARESTD/API/RUN}
PORT=${3:-3000}

VEIT_IS_ROOT_DIR=/opt/veit-is
VEIT_IS_CORE_DIR="${VEIT_IS_ROOT_DIR}/core"
VEIT_IS_USER=veit-is
APACHE_CONFIG=/etc/httpd/conf.d/root.conf
# Path to the old apache configuration file for jira - it's replaced by root.conf
# which contains the ProxyPass commands in proper ordering
OLD_JIRA_CONF=/etc/httpd/conf.d/jira-ajp.conf


echo "--------------------------------------------------------------------------"
echo "Configuring systemd service, creating user, directory structure, firewall rules"
echo "--------------------------------------------------------------------------"
sed -i -e "s@Environment=.*@Environment=\"abraurl=$ABRAURL\"@g;" scripts/systemd/veit-is.service
rsync -avzP --chmod=ugo=rwX scripts/systemd/veit-is.service root@$HOSTNAME:/etc/systemd/system/
rsync -avzP --chmod=ugo=rwX scripts/apache/root.conf root@$HOSTNAME:$APACHE_CONFIG

ssh root@$HOSTNAME << EOF
#reload daemon 
systemctl daemon-reload
# add new veit-is user dedicated for running veit-is web app
useradd -r $VEIT_IS_USER

# Create  directory structure
mkdir -p $VEIT_IS_CORE_DIR
chown -R $VEIT_IS_USER $VEIT_IS_CORE_DIR

# Open given port - used for CORE API documentation
firewall-cmd --zone=public --add-port=$PORT/tcp --permanent
# reload firewal rules to make sure that everything is ok
firewall-cmd --reload

# remove obsolete jira apache config 
rm -f $OLD_JIRA_CONF

# replace variables in apache config - notice the usage of "@" instead of "/" in sed
# this is necessary because variables values contains slashes (directory paths)
sed -i -e "s@{{PORT}}@$PORT@g;" $APACHE_CONFIG

# reload apache config
systemctl reload httpd

# enable systemd service to start it at boot
systemctl enable veit-is.service
EOF

echo "--------------------------------------------------------------------------"
echo "Building distribution"
echo "--------------------------------------------------------------------------"
export LEIN_ROOT=true
lein uberjar

echo "--------------------------------------------------------------------------"
echo "Stopping veit-is core running on $HOSTNAME"
echo "--------------------------------------------------------------------------"
ssh root@$HOSTNAME << EOF
systemctl stop veit-is.service
cd $VEIT_IS_CORE_DIR
if [ -f veit-is.jar ]; then
    cp veit-is.jar veit-is.jar.bak
fi    
EOF

# copy to remote server - notice that file permissions will be set to the defaults on destination server (--chmod=ugo=rwX)
echo "Deploying to $HOSTNAME"
rsync -avzP --chmod=ugo=rwX target/veit-is.jar root@$HOSTNAME:"${VEIT_IS_CORE_DIR}/veit-is.jar"
#rsync -avzP --chmod=ugo=rwX .lein-env root@$HOSTNAME:"${VEIT_IS_CORE_DIR}/.lein-env"



echo "--------------------------------------------------------------------------"
echo "Starting veit-is core running on $HOSTNAME"
echo "--------------------------------------------------------------------------"
ssh root@$HOSTNAME << EOF
systemctl start veit-is.service
journalctl -u veit-is.service | tail
EOF

echo "--------------------------------------------------------------------------"
echo "Checking the deployed application."
echo "Wait for 15 seconds to make sure that application is up & running."
echo "--------------------------------------------------------------------------"
sleep 15
./scripts/api-check.sh $HOSTNAME $PORT

