#!/usr/bin/env bash

if [ -z "$1" ]
then
    echo "Please, specify the working dir path for deployment."
    exit 1;
fi
WORKING_DIR=$1

./deploy-all.sh $WORKING_DIR testis.veit.cz inet.veit.cz http://intranet.veit.cz/ARESTD/API/RUN
