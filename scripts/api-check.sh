# Very basic smoke test for checking if API ping resource responsds OK.
# This should be replaced by more sophisticated smoke tests in the future.

# uncomment to debug bash script
#set -x

HOSTNAME=${1:-testis.veit.cz}
PORT=${2:-3000}
API_URL="http://$HOSTNAME:$PORT/api"

RESPONSE=$(curl -s "$API_URL/smoketests/ping")

if [ $? -ne 0 ]
then
    echo "ERROR: API at $API_URL is not running."
    exit 1;
fi

if [ "$RESPONSE" != "OK" ]
then
    echo "ERROR: API at $API_URL is broken."
    echo "RESPONSE:"
    echo $RESPONSE
    exit 2;
fi

echo "API at $API_URL is up & running."

