function check_code_style() {
  echo
  echo "Running code style checks......."
  echo "======================================================="

  echo
  echo
  echo
  echo "======================================================="
  echo "Running 'lein cljfmt check'"
  echo "======================================================="
  lein cljfmt check

  echo
  echo
  echo
  echo "======================================================="
  echo "Running 'lein bikeshed'"
  echo "======================================================="
  lein bikeshed -m 120

  echo
  echo
  echo
  echo "======================================================="
  echo "Running 'lein kibit'"
  echo "======================================================="
  lein kibit

  echo
  echo
  echo
  echo "======================================================="
  echo "Running 'lein eastwood'"
  echo "======================================================="
  lein eastwood

  echo
  echo
  echo
  echo "======================================================="
  echo "Running 'lein slamhound src'"
  echo "======================================================="
  lein slamhound src

  echo
}

check_code_style
