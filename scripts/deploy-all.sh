#!/usr/bin/env bash
set -e
# uncomment for debugging
#set-x

## Wrapper script which will deploy all applications related to VEIT information system:
## 1. CORE VEIT IS API
## 2. VEIT IS UI 
## 3. VEIT Orders UI (confirmation.veit.cz)
##
## TODO: missing pieces:
## 1. abra-api
## 2. php-ctecka
## 3. confluence-plugins ("Completion norm" macro)
##
## This script will clone the repositories and then execute deploy scripts for each component
## Pass the name of working directory as a first argument
##
## You can specify the hostname where the applications should be deployed as a second argument
## Default hostname is testis.veit.cz

if [ -z "$1" ]
then
    echo "Please, specify the working dir path for deployment."
    exit 1;
fi
WORKING_DIR=$1

HOSTNAME=${2:-testis.veit.cz}
DNSNAME=${3:-inet.veit.cz}
ABRAAPINAME=${4:-http://intranet.veit.cz/ARESTD/API/RUN}

cd $WORKING_DIR

# CORE veit-is
echo 
echo "**************************************************************************"
echo "Deploying veit-is CORE API"
echo "**************************************************************************"
echo 
git clone http://is.veit.cz:7990/scm/is/veit-is.git
cd veit-is
./scripts/deploy.sh $HOSTNAME $ABRAAPINAME

# veit-orders-ui
echo 
echo "**************************************************************************"
echo "Deploying VEIT Orders UI"
echo "**************************************************************************"
echo 
cd .. 
git clone http://is.veit.cz:7990/scm/is/veit-orders-ui.git
cd veit-orders-ui
./scripts/deploy.sh $HOSTNAME $DNSNAME

# veit-is-ui
echo 
echo "**************************************************************************"
echo "Deploying VEIT-IS UI"
echo "**************************************************************************"
echo 
cd .. 
git clone http://is.veit.cz:7990/scm/is/veit-is-ui.git
cd veit-is-ui
./scripts/deploy.sh $HOSTNAME